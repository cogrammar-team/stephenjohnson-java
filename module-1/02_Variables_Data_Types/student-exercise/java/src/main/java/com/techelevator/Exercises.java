package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;

        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;

        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		int racoons = 3;
		int hungryRacoons = racoons-2;
        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int flowers = 5;
		int bees = 3;
		int results =flowers-bees;
        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int lonelyPigeons= 1;
		lonelyPigeons=lonelyPigeons+1;
        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
		int owls = 3;
		int joiningOwls = 2;
		int result = owls+joiningOwls;
        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int beavers = 2;
		int swimmingBeavers = 1;
		int beaversWorkingHome = beavers-swimmingBeavers;

        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
		int toucans = 2;
		int joiningToucans = 1;
		int totalToucans = toucans+joiningToucans;
        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
		int squirrels = 4;
		int nuts = 2;
		int squirrelsWithoutNuts = squirrels-nuts;
        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
        double dime = 0.10;
        double nickels = 0.05;
        double quarter = 0.25;
        double money = dime + 2*nickels + quarter;
        System.out.println(money);

        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */
		int muffinsBriers = 18;
		int muffinsMacAdams = 20;
		int muffinsFlannery = 17;
		int totalMuffins = muffinsBriers+muffinsFlannery+muffinsMacAdams;
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		int hiltYoyo = 24;
		int hiltWhistle =14;
		int hiltTotal = hiltWhistle+hiltYoyo;

        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int RKTLargeMarshmallows = 8;
		int RKTMiniMarshmallows = 10;
		int RKT = RKTLargeMarshmallows+RKTMiniMarshmallows;
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int snowBrecknock = 17;
		int snowHiltHouse = 29;
		int snow = snowBrecknock+snowHiltHouse;
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?

		int hiltToyTruck =3;
		int hiltPencil = 2;
		int hilt = 10 - hiltPencil-hiltToyTruck;
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int joshMarbles = 16;
		int joshLostMarbles = 7;
		joshMarbles=joiningOwls-joshLostMarbles;
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int meganSeashells = 19;
		int meganSeashellsNeedtoFind = 25-meganSeashells;
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int balloonsBrad = 17;
		int balloonsBradGreen = 8;
		int balloonsBradRed = balloonsBrad-balloonsBradGreen;
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int shelfBooks = 38;
		int martaShelfBooks = 10;
		shelfBooks=shelfBooks+martaShelfBooks;
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int legsBee = 6;
		bees = 8;
		int totalLegs =legsBee*bees;
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		double iceCreamCost = 0.99;
		double total = iceCreamCost*2;
		System.out.println(total);
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int totalRocksNeeded =125;
		int hasRocks = 64;
		int rocksNeeded = totalRocksNeeded-hasRocks;
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
			int hiltMarbles = 38;
			int lostMarbles = 15;
			int marblesLeft = hiltMarbles-lostMarbles;
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int concertDistance = 78;
		int milesDrove = 32;
		int milesLeft = concertDistance+milesDrove;

        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		int minsShovelingSaturday = 90;
		int minsShovelingSunday = 45;
		int minsShoveling = minsShovelingSaturday+minsShovelingSunday;
        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		double hotDogs = 0.50;
		int numHotDogs = 6;
		double amtPaid = hotDogs*numHotDogs;
		System.out.println(amtPaid);
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
			double amtHilt = 0.50;
			double costPencil = 0.07;
			double numPencils = amtHilt/costPencil;
        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int hiltSawButterflies = 33;
		int hiltSawButterfliesOrange = 20;
		int hiltSawButterfliesRed = hiltSawButterflies-hiltSawButterfliesOrange;
        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		double kateGave = 1.00;
		double amtCostCandy = 0.54;
		double moneyBack = kateGave-amtCostCandy;

        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int MarkTrees = 13;
		int MarkMoreTrees = 12;
		MarkTrees=MarkTrees+MarkMoreTrees;
        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		int hoursInDay = 24;
		int daysJoySeeGrandMa = 2;
		int hoursTillGrandMa = hoursInDay*daysJoySeeGrandMa;
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int kimCousins = 4;
		int gumPerPerson = 5;
		int totalGum = gumPerPerson*kimCousins;
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		double moneyDanGot = 3.00;
		double costCandy = 1.00;
		moneyDanGot=moneyDanGot=costCandy;
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
		int boatsInLake = 5;
		int peoplePerBoat = 3;
		int totalPeople = boatsInLake*peoplePerBoat;
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
		int legosEllenGot = 380;
		int legosLost = 57;
		legosEllenGot=legosEllenGot-legosLost;
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
		int arthurMuffins = 35;
		int totalArtherMuffins = 83;
		int muffinsStillNeeded = totalArtherMuffins-arthurMuffins;
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
		int totalCrayonsWilly = 1400;
		int totalCrayonsLucy = 290;
		int willMoreCrayonsThanLucy = totalCrayonsWilly-totalCrayonsLucy;
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
		int stickersOnPage = 10;
		int numPages = 22;
		int totalStickers = stickersOnPage*numPages;
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		int totalCupcakes = 96;
		int numKids = 8;
		double cupcakesPerPerson = totalCupcakes/numKids;
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int totalGingerBread = 47;
		int cookiesPerJar = 6;
		int leftOverCookies = totalGingerBread%cookiesPerJar;
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
		int totalCroissants = 59;
		int numPerNeighbor = 8;
		int croissantsLeftOver = totalCroissants%numPerNeighbor;
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
		int totalOutmealCookies = 276;
		int cookiesPerTray = 12;
		int numTrays = totalOutmealCookies/cookiesPerTray;

        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
		int pretzels = 480;
		int servingSize = 12;
		int numServings = pretzels/servingSize;

        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int lemonCupCakes = 53;
		int cakesPerBox = 3;
		int cakesLeftHome = 2;
		int cakesBrought = (lemonCupCakes-cakesLeftHome)/cakesPerBox;
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
		int carrotSticks = 74;
		int peopleForCarrots = 12;
		int carrotsPerPerson = carrotSticks/peopleForCarrots;
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int totalTeddyBears = 98;
		int bearsShelf = 7;
		int shelvesFilled = totalTeddyBears/bearsShelf;
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
			int picturesAlbum = 20;
			int totalPictures = 480;
			int albumsNeeded = totalPictures/picturesAlbum;
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
			int tradingCards = 94;
			int cardsPerBox = 8;
			int numberInLeftoverBox = tradingCards%cardsPerBox;
			int numBoxes = (tradingCards-numberInLeftoverBox)/cardsPerBox;
		System.out.println(numBoxes);
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
			int totalBooks = 210;
			int shelves = 10;
			int booksPerShelf = totalBooks/shelves;
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
			int croissants = 17;
			int numGuests = 7;
			double croissantsPerGuest = croissants/numGuests;
        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
			double billHourlyRate = 1/2.15;
			double jillHourlyRater = 1/1.9;
			double totalHourlyRate = jillHourlyRater+billHourlyRate;
			double amountRoomsPerDay = totalHourlyRate*8;
			double numRoomsNeedToBePainted = 5;
			double daysNeededToPaint = numRoomsNeedToBePainted/amountRoomsPerDay;
		System.out.println("Amount of hours needed for 5 rooms: "+(daysNeededToPaint*8));
			numRoomsNeedToBePainted = 623;
		daysNeededToPaint = numRoomsNeedToBePainted/amountRoomsPerDay;
		System.out.println("Amount of hours needed for 623 rooms: "+(daysNeededToPaint*8));
        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
		String firstName = "Stephen ";
		String lastName = "Johnson ";
		String midInitial = "L.";
		String fullName = lastName + firstName +midInitial;
		System.out.println(fullName);
        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		double totalDistance =800;
		double distanceTraveled = 537;
		int percentCompleted = (int)(100*(distanceTraveled/totalDistance));
		System.out.println(percentCompleted);

	}

}
